/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.proyecto.pservicio.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 *
 * @author Cris
 */
@Entity
public class Mensaje {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Date fecha;
    private String contenido;
    //private Conversacion conversacionId;

}
