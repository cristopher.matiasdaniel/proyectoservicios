package cl.proyecto.pservicio.repository;
import java.util.List;

import cl.proyecto.pservicio.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    List<Usuario> findByCorreo(String email);

}
