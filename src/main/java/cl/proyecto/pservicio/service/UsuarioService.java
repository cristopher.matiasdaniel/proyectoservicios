package cl.proyecto.pservicio.service;

import cl.proyecto.pservicio.entity.Usuario;
import cl.proyecto.pservicio.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {
    @Autowired UsuarioRepository usuarioRepository;
    public List<Usuario> obtenerUsuarios(){
        return usuarioRepository.findAll();
    }
}
